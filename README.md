# Accounting of finance application
Application that exist for personal accounting of finances. You can use it for creating different accounts, prepare categories with balance and adding income/outcome that you can easily follow by. 

## Tech stack
- Angular 18
- TypeScript
- RxJs
- NgRx store management (with effects and entities)
- TailwindCSS
- SCSS

## Install
- `npm run install`
- `npm install json-server`

## Run and watch
- `npm run server` - will run json-server that required for the application
- `npm run start` - will run Angular project
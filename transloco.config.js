module.exports = {
  rootTranslationsPath: 'src/assets/i18n/',
  langs: ['en', 'ru', 'de', 'fr', 'es'],
  keysManager: {}
};

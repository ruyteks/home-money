## 2.0.0
Updated version of Product, that includes of:
- Last version of Angular (18)
- NgRx state management
- Tailwind usage
- TypeScript in full power of usage
- Refactored the whole logic of application with following the modern approaches
- Other stuff

## 1.0.0
Old version of Product that used Angular 12, Akita state management, custom components and not relevant tech approaches